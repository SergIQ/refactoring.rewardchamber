﻿namespace Refactoring.RewardChamber.Core.Bonus
{
    public class MultiplyBonus : RewardBonus
    {
        public int Multiplier { get; private set; }

        public MultiplyBonus(int multiplier)
        {
            Multiplier = multiplier;
        }

        public override void Apply(Chest chest)
        {
            chest.Reward *= Multiplier;
        }
    }
}