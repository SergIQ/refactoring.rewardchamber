﻿namespace Refactoring.RewardChamber.Core.Bonus
{
    public class FreeSpinBonus : RewardBonus
    {
        private readonly int _spinCount;

        public FreeSpinBonus(int spinCount)
        {
            _spinCount = spinCount;
        }

        public override void Apply(Chest chest)
        {
            chest.Reward = 0;
            chest.FreeSpins += _spinCount;
        }
    }
}