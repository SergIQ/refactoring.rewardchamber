﻿namespace Refactoring.RewardChamber.Core.Bonus
{
    public abstract class RewardBonus
    {
        public abstract void Apply(Chest chest);
    }
}