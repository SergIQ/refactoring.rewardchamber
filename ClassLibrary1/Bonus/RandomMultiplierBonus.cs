﻿using System;

namespace Refactoring.RewardChamber.Core.Bonus
{
    public class RandomMultiplierBonus : RewardBonus
    {
        private readonly double _radius;
        private readonly bool _positiveOnly;
        private readonly Random _random;

        public RandomMultiplierBonus(double radius, bool positiveOnly = false)
        {
            _radius = radius;
            _positiveOnly = positiveOnly;

            _random = new Random();
        }
        
        public override void Apply(Chest chest)
        {
            var minPoint = _positiveOnly ? 0 : (-1 * _radius);
            var maxPoint = _radius;

            var randomPoint = minPoint + (maxPoint - minPoint) * _random.NextDouble();

            chest.Reward = (int) Math.Round(chest.Reward * randomPoint);
        }
    }
}