﻿using System;
using System.Collections.Generic;
using System.Linq;
using Refactoring.RewardChamber.Core.Bonus;

namespace Refactoring.RewardChamber.Core.Generator
{
    public class ChestGenerator
    {
        private readonly List<BonusGenerationRule> _rules;
        private readonly Random _random;

        public ChestGenerator(List<BonusGenerationRule> rules, int baseReward, int baseRewardWeight, int emptyRewardWeight)
        {
            _rules = rules;
            _random = new Random();

            BaseReward = baseReward;
            BaseRewardWeight = baseRewardWeight;
            EmptyRewardWeight = emptyRewardWeight;
        }

        public Chest Generate()
        {

            var bonusWeight = _rules.Sum(r => r.FrequencyWeight);
            var randomValue = _random.Next(bonusWeight + BaseRewardWeight + EmptyRewardWeight);

            var ruleBoundary = -1;
            foreach (var rule in _rules)
            {
                ruleBoundary += rule.FrequencyWeight;

                if (ruleBoundary >= randomValue)
                {
                    return PackChest(rule.Bonus);
                }
            }

            return new Chest{FreeSpins = 0, Reward = 0};
        }

        private Chest PackChest(RewardBonus bonus)
        {
            var chest = new Chest
            {
                Reward = BaseReward
            };

            bonus.Apply(chest);

            return chest;
        }
        
        public int BaseReward { get; private set; }

        public int BaseRewardWeight { get; private set; }

        public int EmptyRewardWeight { get; private set; }
    }
}
