﻿using Refactoring.RewardChamber.Core.Bonus;

namespace Refactoring.RewardChamber.Core.Generator
{
    public class BonusGenerationRule
    {
        public BonusGenerationRule(RewardBonus bonus, int weight)
        {
            Bonus = bonus;
            FrequencyWeight = weight;
        }

        public RewardBonus Bonus { get; set; }

        public int FrequencyWeight { get; set; }
    }
}