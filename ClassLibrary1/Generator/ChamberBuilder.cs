﻿using System.Collections.Generic;
using Refactoring.RewardChamber.Core.Bonus;

namespace Refactoring.RewardChamber.Core.Generator
{
    public class ChamberBuilder
    {
        public List<BonusGenerationRule> Bonuses { get; set; }

        public int SpinCount { get; set; }

        public int BaseReward { get; set; }

        public int BaseRewardWeight { get; set; }

        public int EmptyScoreWeight { get; set; }

        public int ChestCount { get; set; }

        public ChamberBuilder()
        {
            Bonuses = new List<BonusGenerationRule>();
            SpinCount = 1;
            ChestCount = 1;
            BaseReward = 100;
            BaseRewardWeight = 1000;
            EmptyScoreWeight = 5000;
        }

        public ChamberBuilder AddRule(RewardBonus bonus, int weight)
        {
            Bonuses.Add(new BonusGenerationRule(bonus, weight));

            return this;
        }

        public ChamberBuilder AddMultiplier(int multiplier, int weight)
        {
            return AddRule(new MultiplyBonus(multiplier), weight);
        }

        public ChamberBuilder AddRandomMultiplier(double radius, int weight)
        {
            return AddRule(new RandomMultiplierBonus(radius), weight);
        }

        public ChamberBuilder AddFreeSpin(int spinCount, int weight)
        {
            return AddRule(new FreeSpinBonus(spinCount), weight);
        }

        public ChamberBuilder SetSpins(int spinCount)
        {
            SpinCount = spinCount;

            return this;
        }

        public ChamberBuilder SetBaseReward(int reward, int weight)
        {
            BaseReward = reward;
            BaseRewardWeight = weight;

            return this;
        }

        public ChamberBuilder SetEmptyScore(int weight)
        {
            EmptyScoreWeight = weight;

            return this;
        }

        public ChamberBuilder SetChestCount(int chestCount)
        {
            ChestCount = chestCount;

            return this;
        }

        public Chamber Build()
        {
            return new Chamber(ChestCount, SpinCount, Bonuses, BaseReward, BaseRewardWeight, EmptyScoreWeight);
        }
    }
}