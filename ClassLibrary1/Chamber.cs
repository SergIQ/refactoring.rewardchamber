﻿using System;
using System.Collections.Generic;
using System.Linq;
using Refactoring.RewardChamber.Core.Bonus;
using Refactoring.RewardChamber.Core.Generator;

namespace Refactoring.RewardChamber.Core
{
    public class Chamber
    {
        public List<Chest> Chests { get; }

        public int SpinsLeft { get; set; }

        public int Score { get; set; }

        public int BaseReward { get; private set; }

        public int BaseRewardWeight { get; private set; }

        public int EmptyRewardWeight { get; private set; }

        private readonly int _chestCount;
        private readonly List<BonusGenerationRule> _rules;
        private readonly Random _random;

        public Chamber(int chestCount, int spinsLeft, List<BonusGenerationRule> rules, int baseReward, int baseRewardWeight, int emptyRewardWeight)
        {
            Chests = new List<Chest>(chestCount);
            _chestCount = chestCount;
            SpinsLeft = spinsLeft;
            _rules = rules;
            _random = new Random();

            BaseReward = baseReward;
            BaseRewardWeight = baseRewardWeight;
            EmptyRewardWeight = emptyRewardWeight;
        }

        public bool Spin()
        {
            if (SpinsLeft == 0) return false;

            Chests.Clear();

            for (var i = 0; i < _chestCount; i++)
            {
                var bonusWeight = _rules.Sum(r => r.FrequencyWeight);
                var randomValue = _random.Next(bonusWeight + BaseRewardWeight + EmptyRewardWeight);

                var ruleBoundary = -1;
                BonusGenerationRule targetRule = null;
                foreach (var rule in _rules)
                {
                    ruleBoundary += rule.FrequencyWeight;

                    if (ruleBoundary >= randomValue)
                    {
                        targetRule = rule;
                        break;
                    }
                }

                if (targetRule != null)
                {
                    var chest = new Chest
                    {
                        Reward = BaseReward
                    };

                    targetRule.Bonus.Apply(chest);

                    Chests.Add(chest);
                }
                else
                {
                    var chest = new Chest {FreeSpins = 0, Reward = 0};
                    Chests.Add(chest);
                }
            }

            return true;
        }

        public void Open(int number)
        {
            var chest = Chests[number];

            SpinsLeft--;

            Score += chest.Reward;
        }

        public Chest Generate()
        {

            var bonusWeight = _rules.Sum(r => r.FrequencyWeight);
            var randomValue = _random.Next(bonusWeight + BaseRewardWeight + EmptyRewardWeight);

            var ruleBoundary = -1;
            foreach (var rule in _rules)
            {
                ruleBoundary += rule.FrequencyWeight;

                if (ruleBoundary >= randomValue)
                {
                    var chest = new Chest
                    {
                        Reward = BaseReward
                    };

                    rule.Bonus.Apply(chest);

                    return chest;
                }
            }

            return new Chest { FreeSpins = 0, Reward = 0 };
        }
    }
}
