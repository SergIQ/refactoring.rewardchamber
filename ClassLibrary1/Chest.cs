﻿namespace Refactoring.RewardChamber.Core
{
    public class Chest
    {
        public int Reward { get; set; }

        public int FreeSpins { get; set; }
    }
}