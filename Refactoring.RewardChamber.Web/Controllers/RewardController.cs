﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Refactoring.RewardChamber.Core;
using Refactoring.RewardChamber.Core.Generator;
using Refactoring.RewardChamber.Web.Models;

namespace Refactoring.RewardChamber.Web.Controllers
{
    public class RewardController : Controller
    {
        private static Chamber _chamber;
        private static int? _openedChest;

        [HttpPost]
        public ActionResult Start(int spinCount, int baseReward)
        {
            var builder = new ChamberBuilder()
                .SetSpins(spinCount)
                .SetChestCount(3)
                .SetBaseReward(baseReward, 580)
                .AddMultiplier(2, 100)
                .AddMultiplier(3, 70)
                .AddMultiplier(5, 50)
                .AddRandomMultiplier(0.5, 100)
                .AddRandomMultiplier(1, 50)
                .AddFreeSpin(1, 50)
                .SetEmptyScore(300);

            _chamber = builder.Build();

            return Spin();
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = BuildModel();

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult Open(int chestNumber)
        {
            _chamber.Open(chestNumber);
            _openedChest = chestNumber;

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Spin()
        {
            _chamber.Spin();
            _openedChest = null;

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Complete()
        {
            return View("Result", BuildModel());
        }

        private RewardChamberModel BuildModel()
        {
            var model = new RewardChamberModel
            {
                SpinCount = _chamber?.SpinsLeft ?? 0,
                TotalScore = _chamber?.Score ?? 0,

                Chests = _chamber?.Chests.Select(c => new ChestModel
                {
                    Chest = c,
                    IsOpen = _openedChest.HasValue,
                    IsSelected = _chamber.Chests.IndexOf(c) == _openedChest
                }).ToList() ?? new List<ChestModel>()
            };

            return model;
        }
    }
}
