﻿using Refactoring.RewardChamber.Core;

namespace Refactoring.RewardChamber.Web.Models
{
    public class ChestModel
    {
        public Chest Chest { get; set; }

        public bool IsOpen { get; set; }

        public bool IsSelected { get; set; }
    }
}
