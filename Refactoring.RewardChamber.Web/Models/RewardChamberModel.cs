﻿using System.Collections.Generic;

namespace Refactoring.RewardChamber.Web.Models
{
    public class RewardChamberModel
    {
        public int SpinCount { get; set; }

        public int TotalScore { get; set; }

        public List<ChestModel> Chests { get; set; }
    }
}
